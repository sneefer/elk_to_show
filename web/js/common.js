$(function(){
	
	$("#ready_btn").click(function(){
		var html = "";
		if (data instanceof Object)
		{
			$(".product_sel_chk").each(function(){
				if ($(this).is(":checked"))
				{
					var dat = $("#product_html").html();
					var id = $(this).val();
					dat = dat.replace(/{#id#}/gi,id);
					dat = dat.replace(/{#name#}/gi,data[id].title);
					dat = dat.replace(/{#price_sell#}/gi,data[id].price_sell);
					
					html += dat;
					
					$(this).parent().parent().hide();
				}
			});
			
			$(".product_sel_chk").prop("checked",false);
			
			$("#table_products tbody").append(html);
		}
	});
	
	
	$(".prod_ajax").submit(function(e){
		var $form = $(this);
		$.ajax({
			url: $form.attr("action"),
			type: "post",
			data: $form.serialize(),
			async: false,
			success: function(data)
			{
				if (data != "0")
				{
					$("#cart_count").html("Корзина ("+data+")");
					$form.parent().parent().fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300);
				}
				else
				{
					$("#cart_count").html("Корзина (пусто)");
				}
				
			}
		});
		
		
		return false;
	});
	
	$("#cart-form").submit(function(e){
		var error = false;
		$(this).find("input").each(function(){
			if ($(this).attr("required") == "required" && $(this).length == 0)
			{
				error = true;
			}
		});
		
		if (error)
		{
			return false;
		}
	});
	
	$(".number").keyup(function(){
		var num = $(this).val().replace(/[^\d;]/g, '');
		$(this).val(num);
	});
	
	$(".cart_number").keyup(function(){
		var num = parseInt($(this).val());
		if (num > 0)
		{
			var summ_one = parseFloat($(this).parent().next().html());
			$(this).parent().next().next().html((summ_one*num) + " BYN");
		}
	});
});