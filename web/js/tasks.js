$(document).ready(function(){
	$('.datepicker').datetimepicker({
		format: 'yyyy-mm-dd hh:ii',
		language: 'ru'
	});
	
});

function deleteTask(tid)
{
	if (confirm("Удалить?")) {

		if (tid > 0)
		{
			$.ajax({
				url: "/ajax-site/remove-task",
				data: {id: tid},
				type: "post",
				success: function(data){
					if (data == "1")
					{
						window.location.reload();
					}
					else
					{
						alert("Ошибка при удалении");
					}
				}
			});
		}
	}
}