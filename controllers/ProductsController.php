<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Product;

class ProductsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    /*public function getStorages()
    {
    	// a customer has many comments
    	return $this->hasMany(Strorages::className(), ['storeid' => 'id']);
    }*/

    public function actionIndex()
    {
        $prod_obj = new Product();
        $data_products = $prod_obj->getProducts(0);
        //print_r($data_products[0]);
        
        return $this->render('index',array('products' => (array)$data_products));
    }

    public function actionView($id)
    {
        if ($id > 0)
        {
            $prod_obj = new Product();
            $data_products = $prod_obj->getProduct($id);
            return $this->render('view',array('product' => (array)$data_products));
        }
    }

	public function actionAddToCart($id)
	{
		$cart = new ShoppingCart();
	
		$model = Product::findOne($id);
		if ($model) {
			$cart->put($model, 1);
			return $this->redirect(['cart-view']);
		}
		throw new NotFoundHttpException();
	}

    public function actionCart()
    {
        return $this->render('cart',array('product' => (array)$data_products));
    }
}