<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

use app\models\BackendUser;
use app\models\Storages;
use app\models\Product;
use app\models\Order;
use app\models\StoragesProducts;

use app\models\Constants;
use yii\helpers\ArrayHelper;



class ManageController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
        'access' => [
            'class' => \yii\filters\AccessControl::className(),
            'only' => ['orders'],
            'rules' => [
	            [
	            'allow' => true,
	            'roles' => ['@'],
	            ],
            ],
        ]
       ];
	}
	
	public function actions()
	{
		return [];
	}
	
	public function actionIndex($id = 0)
	{
		if (Yii::$app->user->identity->haveUserOnlyRights())
		{
			return $this->redirect(['manage/orders']);
		}
		else
		{
			$prod_obj = new Product();
			if ($id == 0)
			{
    			$data_products = $prod_obj->getProducts(0);
			}
			else
			{
				$data_products = $prod_obj->getStorageProducts($id);
			}
    		
    		return $this->render('index',array('products' => (array)$data_products));
		}
	}
	
	public function actionOrders()
	{
		$orders_obj = Order::find()->where(['userid' => Yii::$app->user->identity->id])->orderBy(['updated' => SORT_DESC])->all();
		return $this->render('orders',['orders' => $orders_obj]);
	}
	
	public function actionManage_orders()
	{
		if (Yii::$app->user->identity->haveUserOnlyRights())
		{
			return $this->redirect(['manage/orders']);
		}
		else {
			$orders_obj = array();
			
			if (Yii::$app->user->identity->type == Constants::$user_type_admin) {
				$orders_obj = Order::find()->orderBy(['updated' => SORT_DESC])->all();
			}

			if (Yii::$app->user->identity->type == Constants::$user_type_manager) {
				$orders_obj = Order::find()->where(['userid' => Yii::$app->user->identity->id])->orderBy(['updated' => SORT_DESC])->all();
			}

			return $this->render('manage_orders', ['orders' => $orders_obj]);
		}
	}
	
	public function actionManage_order_status($id)
	{
		if (Yii::$app->user->identity->haveUserOnlyRights())
		{
			return $this->redirect(['manage/orders']);
		}
		else
		{
			$posts = \Yii::$app->request->post();
			
			if (isset($posts['order_status']))
			{
				if ($id > 0)
				{
					//$product = Product::find(['id' => $id])->one();
					$order = Order::findOne(['id' => $id]);
					
					if ($order != null)
					{
						$order->status = $posts['order_status'];
						$order->save();
					}
				}
			}
			
			return $this->redirect(['/manage/manage_orders']);
		}
	}
	
	public function actionManage_order_edit($id)
	{
		if (Yii::$app->user->identity->haveUserOnlyRights())
		{
			return $this->redirect(['manage/orders']);
		}
		else
		{
			$get = Yii::$app->request->get();
				
			if (isset($get['id']))
			{
				$order = Order::find()->where(['id' => $get['id']])->one();
					
				return $this->render('order_edit', ['model' => $order]);
			}
			else
			{
				return $this->redirect(['manage/orders']);
			}
		}
	}
	
	public function actionManage_order_edit_do($id)
	{
		if (Yii::$app->user->identity->haveUserOnlyRights())
		{
			return $this->redirect(['manage/orders']);
		}
		else
		{
			$posts = \Yii::$app->request->post();
			
			$order = Order::find()->where(['id' => $id])->one();
			
			if ($order != null && array_key_exists("prods",$posts)
				&& array_key_exists("prods_summ",$posts) )
			{
				$prods = array();
				$prods_ids = [];
				$prods_old = json_decode($order->products, true);
				/*$old_count = 0;
				foreach($prods_old as $id=>$prod)
				{
					$old_count += $prod[0];
				}*/
				$total_amount = 0;
				//$diff = 0;
				//$new_count = 0;
				foreach($posts['prods'] as $id => $val)
				{
					//$new_count += $val;
					$prods_ids[] = $id;
					$prods[$id] = array($val, $posts['prods_summ'][$id]);
					$total_amount += $val * floatval($posts['prods_summ'][$id]);
					
					if (is_array($prods_old))
					{
						if ($prods_old[$id][0] - $val != 0)
						{
							//Обновляем на складе инфу о количестве товаров:
							$stpr = StoragesProducts::findOne(['storage_id' => $order->storage_id, 'product_id' => $id]);
							if ($stpr != null)
							{
								$stpr->count += ($prods_old[$id][0] - $val);
								$stpr->save();
							}
							
						}
					}
				}
				
				if (count($posts['prods']) != count($prods_old))
				{
					$diff_array = array();
					foreach($prods_old as $id => $val)
					{
						$found = false;
						foreach($posts['prods'] as $id2 => $val2)
						{
							if ($id == $id2)
							{
								$found = true;
								break;
							}
						}
						if (!$found)
						{
							//Обновляем на складе инфу о количестве товаров:
							$stpr = StoragesProducts::findOne(['storage_id' => $order->storage_id, 'product_id' => $id]);
							if ($stpr != null)
							{
								$stpr->count += $val[0];
								$stpr->save();
							}
						}
					}
				}
				
				//добавляем новые продукты
				//$prods = array();
				if (array_key_exists("new_prods",$posts))
				{
					foreach($posts['new_prods'] as $id => $val)
					{
						//$new_count += $val;
						$prods_ids[] = $id;
						$prods[$id] = array($val, $posts['new_prods_summ'][$id]);
						$posts['prods'][$id] = $val;
						$posts['prods_summ'][$id] = $posts['new_prods_summ'][$id];
						$total_amount += $val * floatval($posts['new_prods_summ'][$id]);
							
						
						//Обновляем на складе инфу о количестве товаров:
						$stpr = StoragesProducts::findOne(['storage_id' => $order->storage_id, 'product_id' => $id]);
						if ($stpr != null)
						{
							$stpr->count += $val;
							$stpr->save();
						}
					}
				}
				
				//$diff = $new_count - $old_count;
				$order->products = json_encode($prods);
				//echo $order->products;
				$order->summ = $total_amount;
				
				$order->save();
				
				//ЗДЕСЬ можно написать интеграцию:
				$products = Product::findAll(['id' => $prods_ids]);
				$request = $build =  array();
				$request['action'] = "update_order";
				$build['order'] = array();
				$ind = 0;
				foreach($products as $product)
				{
					$build['order'][$ind]['sku_code'] = $product->artid;
					$build['order'][$ind]['name'] = $product->title;
					$build['order'][$ind]['purchase_price'] = $product->price_buy;
					$build['order'][$ind]['price'] = $posts['prods_summ'][$product->id];
					$build['order'][$ind]['quantity'] = $posts['prods'][$product->id];
					$ind++;
				}
				$build['id'] = $order->id;
				$request['data'] = json_encode($build);
				
				
				
				$ch = curl_init("https://elksale.xyz/elksite.php");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_TIMEOUT, 20);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
				$body = curl_exec($ch);
				curl_close($ch);
				
				return $this->redirect(['manage/manage_orders']);
			}
		}
	}
	
	public function actionManage_order_delete($id)
	{
		if (Yii::$app->user->identity->haveUserOnlyRights())
		{
			return $this->redirect(['manage/orders']);
		}
		else
		{
			$get = Yii::$app->request->get();
			
			if (isset($get['id']))
			{
				$model = Order::find()->where(['id' => $get['id']])->one();
				$model->delete();
			
				return $this->redirect(['manage/manage_orders']);
			}
			else
			{
				return $this->redirect(['manage/orders']);
			}
		}
	}
	
	//products
	public function actionManageProducts()
	{
		// manage/index   как замена данной функции


		if (Yii::$app->user->identity->haveUserOnlyRights())
		{
			return $this->redirect(['manage/orders']);
		}
		else
		{
			
		}
	}
	
	public function actionProducts_add()
	{
		if (Yii::$app->user->identity->haveUserOnlyRights())
		{
			return $this->redirect(['manage/orders']);
		}
		else
		{
			$prod_obj = new Product();
			$store_obj = new Storages();
			$stores = (array)$store_obj->find()->asArray()->all();
			
			$prod_obj->storages = $stores;
			
			//$stores = ArrayHelper::getColumn($stores,"storagename");
			//$stores = array_merge($stores_fin,$stores);
		
			return $this->render('add', ['model' => $prod_obj, 'stores' => $stores]);
			
		}
	}
	
	public function actionProducts_add_do()
	{
		if (Yii::$app->user->identity->haveUserOnlyRights())
		{
			return $this->redirect(['manage/orders']);
		}
		else
		{
			$model = new Product();
			$model->load(\Yii::$app->request->post());
				
			$posts = \Yii::$app->request->post();
			
			
			if ($model->validate())
			{
				$file = UploadedFile::getInstance($model, 'photo');
				if ($file)
				{
					$fname = md5($file->baseName . time());
					$file->saveAs('img/' . $fname . '.' . $file->extension);
					$posts['Product']['photo'] = $fname . "." . $file->extension;
				}
				$model->setAttributes($posts['Product']);
				$stat = $model->save();
				 
				if ($stat)
				{
					if (array_key_exists('stores',$posts))
					{
						foreach($posts['stores'] as $id => $store)
						{
							if ($id > 0)
							{
								$stpr = new StoragesProducts();
								$stpr->count = $store['count'] != '' ? intval($store['count']) : 0;
								$stpr->count_real = $store['count_real'] != '' ? intval($store['count_real']) : 0;
								$stpr->product_id = $model->id;
								$stpr->storage_id = $id;
								$stpr->sku = $model->artid;
								
								$stpr->save();
							}
						}
					}
				}
				
				return $this->redirect(['manage/index']);
			}
			else
			{
				$errors = $model->errors;
				$store_obj = new Storages();
				$stores = (array)$store_obj->find()->asArray()->all();
				$stores = ArrayHelper::getColumn($stores,"storagename");
				return $this->render('add',['model' => $model,'errors' => $errors, 'stores' => $stores]);
			}
		}
	}
	
	public function actionProducts_edit()
	{
		$get = Yii::$app->request->get();
		
		if (isset($get['id']))
		{
			//$prod_obj = new Product();
			$prod_obj = Product::findOne(['id' => $get['id']]);
			
			if ($prod_obj)
			{
				$store_obj = new Storages();
				
				$stores = (array)$store_obj->find()->asArray()->all();
				
				$stores_obj = $store_obj->find()->all();
				$stpr = (array)StoragesProducts::find()->asArray()->all();
				
				$prod_obj->storages = $stores;
				
				return $this->render('edit', ['model' => $prod_obj, 'stores' => $stores, 'stores_obj' => $stores_obj, 'stpr' => $stpr]);
			}
		}
		else
		{
			return $this->redirect(['manage/index']);
		}
	}
	
	public function actionProducts_edit_do()
	{
		$get = Yii::$app->request->get();

		if (Yii::$app->user->identity->haveUserOnlyRights())
		{
			return $this->redirect(['manage/orders']);
		}
		else
		{
			if (isset($get['id']))
			{
				$model = Product::findOne(['id' => $get['id']]);
			
				$photo_old = $model->photo;
				
				$model->load(\Yii::$app->request->post());
			
				$posts = \Yii::$app->request->post();
				
				if ($model->validate())
				{
					$file = UploadedFile::getInstance($model, 'photo');
					
					if (!empty($file))
					{
						$fname = md5($file->baseName . time());
						$file->saveAs('img/' . $fname . '.' . $file->extension);
						$posts['Product']['photo'] = $fname . "." . $file->extension;
						
						//delete old file:
						if (strlen($photo_old) > 0)
						{
							if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/img/".$photo_old))
							{
								unlink($_SERVER['DOCUMENT_ROOT'] . "/img/".$photo_old);
							}
						}
					}
					else
					{
						$posts['Product']['photo'] = $photo_old;
					}
					
					$model->setAttributes($posts['Product']);
					$stat = $model->save();
					
					if ($stat)
					{
						$stprs = StoragesProducts::find()->where(["product_id" => $model->id])->all();
					
						foreach($posts['stores'] as $id => $store)
						{
							$found = false;
							if ($id > 0)
							{
								//$stpr = new StoragesProducts();
								foreach($stprs as $stpr)
								{
									if ($stpr->storage_id == $id && $stpr->product_id == $model->id)
									{
										$stpr->count = $store['count'] != '' ? intval($store['count']) : 0;
										$stpr->count_real = $store['count_real'] != '' ? intval($store['count_real']) : 0;
										$stpr->product_id = $model->id;
										//$stpr->storage_id = $id;
										$stpr->sku = $model->artid;
										
										$stpr->save();
										$found = true;
									}
								}
								
								if(!$found)
								{
									$stpr = new StoragesProducts();
									$stpr->count = $store['count'] != '' ? intval($store['count']) : 0;
									$stpr->product_id = $model->id;
									$stpr->storage_id = $id;
									$stpr->sku = $model->artid;
									
									if ($stpr->validate())
									{
										$stpr->save();
										/*print_r($stpr);
										die();*/
									}
									else
									{
										
										//print_r($stpr->errors);
										//die();
									}
								}
							}
						}
					}
			
					return $this->redirect(['manage/index']);
				}
				else
				{
					$errors = $model->errors;
					$store_obj = new Storages();
					$stores = (array)$store_obj->find()->asArray()->all();
					$stores = ArrayHelper::getColumn($stores,"storagename");
					return $this->render('edit',['model' => $model,'errors' => $errors, 'stores' => $stores]);
				}
			}
			else
			{
				return $this->redirect(['manage/index']);
			}
		}
	}
	
	public function actionProducts_delete_do()
	{
		$get = Yii::$app->request->get();
		
		if (isset($get['id']))
		{
			$model = Product::find()->where(['id' => $get['id']])->one();
			$model->delete();
			
			return $this->redirect(['manage/index']);
		}
		else
		{
			return $this->redirect(['manage/index']);
		}
	}
	
	//storages:
	public function actionStorages()
	{
		if (Yii::$app->user->identity->haveUserOnlyRights())
		{
			return $this->redirect(['manage/orders']);
		}
		else
		{
			$storages_obj = new Storages();
			$stor = (array)$storages_obj->find()->all();
			//print_r($stor);
			return $this->render('storage' ,['storages' => $stor]);
		}
	}
	
	public function actionStorages_add()
	{
		$store = new Storages();
		return $this->render('storage_add', ['model' => $store]);
	}
	
	public function actionStorages_add_do()
	{
		$model = new Storages();
		$model->load(\Yii::$app->request->post());
		 
		$posts = \Yii::$app->request->post();
		 
		if ($model->validate()) {
			
			$model->setAttributes($posts['Storages']);
			$stat = $model->save();
			
			if ($stat)
			{
				//синхронизируем склады для продуктов:
				$products = Product::find()->all();
				//$storages = Storages::find()->all();
			
				foreach($products as $product)
				{
					$stpr_n = new StoragesProducts();
					$stpr_n->sku = $product->artid;
					$stpr_n->product_id = $product->id;
					$stpr_n->storage_id = $model->id;
					$stpr_n->count = 0;
					$stpr_n->count_real = 0;
					$stpr_n->save();
				}
			
			}
			return $this->redirect(['manage/storages']);
		}
		else
		{
			// вывод ошибок при регистрации
			$errors = $model->errors;
			return $this->render('storage_add',['model' => $model,'errors' => $errors]);
		}
	}
	
	public function actionStorage_edit($id)
	{
		if (Yii::$app->user->identity->haveUserOnlyRights())
		{
			return $this->redirect(['manage/orders']);
		}
		else
		{
			if (intval($id) > 0)
			{
				$storage = Storages::findOne(['id' => $id]);
				$stprs = StoragesProducts::find()->where(["storage_id" => $id])->all();
				$stprs_arr = array();
				foreach($stprs as $stpr)
				{
					$stprs_arr[$stpr->product_id] = $stpr;
				}
				$prod_obj = new Product();
				$data_products = $prod_obj->getStorageProducts($id);
				
				return $this->render('storage_edit',['model' => $storage,'products' => $data_products, 'stprs' => $stprs_arr]);
			}
			else
			{
				return $this->redirect(['/manage/storages']);
			}
		}
	}
	
	public function actionStorage_edit_do($id)
	{
		if (Yii::$app->user->identity->haveUserOnlyRights())
		{
			return $this->redirect(['manage/orders']);
		}
		else
		{
			$storage = Storages::findOne(['id' => $id]);
			$posts = \Yii::$app->request->post();
			
			if (array_key_exists("prods",$posts))
			{
				if (is_array($posts["prods"]))
				{
					foreach($posts["prods"] as $id_p=>$one)
					{
						$product = Product::findOne(['id' => $id_p]);
						if ($product != null)
						{
							$stpr = StoragesProducts::findOne(['storage_id' => $id, 'product_id' => $id_p]);
							if ($stpr != null)
							{
								$stpr->count = $one;
								if (!empty($posts['prods_summ'][$id_p]))
								{
									$product->price_sell = floatval($posts['prods_summ'][$id_p]);
								}
								$stpr->save();
								$product->save();
							}
						}
					}
					return $this->redirect(['manage/storages']);
				}
			}
			return $this->redirect(['manage/storages']);
		}
	}
	
	public function actionStorages_delete_do()
	{
		$get = Yii::$app->request->get();
		
		if (isset($get['id']))
		{
			$model = Storages::find()->where(['id' => $get['id']])->one();
			$model->delete();
				
			return $this->redirect(['manage/storages']);
		}
		else
		{
			return $this->redirect(['manage/storages']);
		}
	}
}