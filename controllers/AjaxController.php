<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\Tasks;
use app\models\BackendUser;
use app\models\Product;

class AjaxController extends Controller
{
	public function actionAdd_to_cart()
	{
		if (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
		 
			$post = Yii::$app->request->post();
			
			$posts = Yii::$app->request->post();

			if (strlen($posts['amount']) > 0)
			{
				if (intval($posts['amount']) > 0)
				{
					$model = Product::findOne($posts['id']);
					if ($model) {
						//$cart->put($model, $posts['amount']);
						\Yii::$app->cart->put($model, intval($posts['amount']));
						return \Yii::$app->cart->getCount();
					}
				}
				else
				{
					return "0";
				}
			}
			else
			{
				return "0";
			}
			//print_r($posts);
		 
			return "0";
		}
		else
		{
			return "0";
		}
	}
	
}

?>