<?php

namespace app\controllers;

use app\models\BackendUser;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Tasks;
use app\models\Product;
use app\models\RegisterForm;
use app\models\Constants;
use app\models\Order;
use app\models\Storages;
use app\models\StoragesProducts;
//use app\models\BackendUser;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
    	$prod_obj = new Product();
    	$data_products = $prod_obj->getProducts(0);
    	return $this->render('index',array('products' => (array)$data_products));
		
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    /*public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }*/
    
    public function actionRegister()
    {
    	$model = new RegisterForm();
    	return $this->render('register',['model' => $model, 'errors' => null]);	
    }
    
    public function actionDoregister()
    {
    	$model = new RegisterForm();
    	$model->load(\Yii::$app->request->post());
    	
    	
    	
    	if ($model->validate()) {
    		// регистрация
    		$posts = \Yii::$app->request->post();
    		//выставляем значения:
    		$errors = "";
    		$errors .= BackendUser::findByUsername($posts['RegisterForm']['username']) == NULL ? "" : "Такое имя пользователя уже существует<br />";
    		$errors .= BackendUser::findOne(['unp' => $posts['RegisterForm']['unp'] ]) == NULL ? "" : "Такой УНП уже существует<br />";
    		$errors .= BackendUser::findOne(['email' => $posts['RegisterForm']['email'] ]) == NULL ? "" : "Такой Email уже существует<br />";
    		
    		if (strlen($errors) == 0)
    		{
	    		$reguser = new BackendUser();
	    		$posts['RegisterForm']['type'] = Constants::$user_type_simple;
	    		$posts['RegisterForm']['authKey'] = md5(time());
	    		$posts['RegisterForm']['created'] = time();
	    		$reguser->setAttributes($posts['RegisterForm']);
	    		
	    		$reguser->hashPassword();
	    		$reguser->save(true);
	    		
	    		//$model = new LoginForm();
	    		//return $this->render('login',['model' => $model, 'register' => 1]);
	    		
	    		
	    		
	    		return $this->redirect(['login']);
    		}
    		else
    		{
    			return $this->render('register',['model' => $model,'errors' =>array('username' => array($errors))]);
    		}
    		
    	} else {
    		// вывод ошибок при регистрации
    		$errors = $model->errors;
    		return $this->render('register',['model' => $model,'errors' => $errors]);
    	}
    }
    
    public function actionCart()
    {
    	$model = new RegisterForm();
    	
    	$stores = Storages::find()->all();
    	
    	return $this->render('cart', ['model' => $model, 'errors' => '', 'stores' => $stores]);
    }
    public function actionCart_do()
    {
    	if(Yii::$app->user->isGuest)
    	{
    		//Здесь регистрация при оформлении заказа
    		
    		//Начинаем регистрацию:
    		/*$model = new RegisterForm();
    		$model->load(\Yii::$app->request->post());
    		 
    		if ($model->validate()) {
    			// регистрация
    			$posts = \Yii::$app->request->post();
    			//выставляем значения:
    			if (BackendUser::findByUsername($posts['RegisterForm']['username']) == NULL)
    			{
    				$reguser = new BackendUser();
    				$posts['RegisterForm']['type'] = Constants::$user_type_simple;
    				$posts['RegisterForm']['authKey'] = md5(time());
    				$posts['RegisterForm']['created'] = time();
    				$reguser->setAttributes($posts['RegisterForm']);
    				$reguser->hashPassword();
    				
    				if ($reguser->save(true))
    				{
    					$products = \Yii::$app->cart->getPositions();
				    	$prod_ids = array();
				    	foreach($products as $product)
				    	{
				    		if ($product->quantity <= $product->count)
				    		{
				    			$prod_ids[$product->id] = $product->quantity;
				    			$product->count -= $product->quantity;
				    		}
				    	}
				    	//заказ будет сформирован при условии, что количество покупаемых товаров меньше либо равно остатку на слкаде
				    	if (count($prod_ids) > 0)
				    	{
				    		//минусуем количество товаров:
				    		foreach($prod_ids as $id => $quantity)
				    		{
				    			foreach($products as $product)
				    			{
				    				if ($product->id == $id)
				    				{
				    					$product->save();
				    				}
				    			}
				    			
				    		}
				    		
				    		//Добавляем заказ:
				    		$order = new Order();
				    		
				    		$order->updated = $order->created = date('Y-m-d H:i:s');
				    		$order->products = json_encode($prod_ids);
				    		$order->status = Constants::$order_status_new;
				    		$order->userid = $reguser->id;
				    		$stat = $order->save();
				    	
				    		if($stat)
				    		{
				    			\Yii::$app->cart->removeAll();
				    			return $this->redirect(['/manage/orders']);
				    		}
				    		
				    	}
    				}
    				
    				//return $this->redirect(['/site/cart']);
    				return $this->redirect(['login']);
    			}
    			else
    			{
    				return $this->redirect(['register']);
    			}
    		
    		} else {
    			// вывод ошибок при регистрации
    			$errors = $model->errors;
    			return $this->render('register',['model' => $model,'errors' => $errors]);
    		}
    		
    		//return $this->redirect(['register']);
    		
    		*/
    	}
    	else
    	{
	    	$products = \Yii::$app->cart->getPositions();
	    	$posts = \Yii::$app->request->post();
	    	$stores = Storages::find()->all();
	    	
	    	if (isset($posts['storesel']))
	    	{
	    		$store_sel_id = intval($posts['storesel']);
	    	}
	    	else
	    	{
	    		$store_sel_id = 0;
	    	}
	    	
	    	foreach($stores as $store)
	    	{
	    		if($store->id == $store_sel_id)
	    		{
	    			foreach($products as $product)
	    			{
		    			$stpr = StoragesProducts::findOne(['storage_id' => $store_sel_id, 'product_id' => $product->id]);
		    			if ($stpr)
		    			{
			    			$stpr->count -= $posts['count'][$product->id];
			    			$stpr->save();
		    			}
	    			}
	    			break;
	    		}
	    	}
	    	
	    	$prod_ids = array();
	    	$total_amount = 0;
	    	foreach($products as $product)
	    	{
	    		$prod_ids[$product->id] = array($posts['count'][$product->id],$product->price_sell);
	    		$total_amount += ($posts['count'][$product->id] * $product->price_sell);
	    	}
	    		
	    		
	    	//Добавляем заказ:
	    	$order = new Order();
	    		
	    	$order->updated = $order->created = date('Y-m-d H:i:s');
	    	$order->products = json_encode($prod_ids);
	    	$order->status = Constants::$order_status_new;
	    	$order->storage_id = $store_sel_id;
	    	$order->userid = Yii::$app->user->identity->id;
	    	$order->summ = $total_amount;
	    	$order->unp = $posts['addfield']['unp'];
	    	$order->email = $posts['addfield']['email'];
	    	$order->phone = $posts['addfield']['phone'];
	    	$order->uname = $posts['addfield']['uname'];
	    	$stat = $order->save();
	    	
	    	if($stat)
	    	{
	    		\Yii::$app->cart->removeAll();
	    		
	    		
	    		//ЗДЕСЬ можно написать интеграцию:
	    		$request = $build =  array();
	    		$request['action'] = "create_order";
	    		$build['order'] = array();
	    		$ind = 0;
	    		foreach($products as $product)
	    		{
	    			$build['order'][$ind]['purchase_price'] = $product->price_buy;
	    			$build['order'][$ind]['name'] = $product->title;
	    			$build['order'][$ind]['sku_code'] = $product->artid;
	    			$build['order'][$ind]['price'] = $product->price_sell;
	    			$build['order'][$ind]['quantity'] = $posts['count'][$product->id];
	    			$ind++;
	    		}
	    		$build['name'] = strlen($order->uname) > 0 ? $order->uname : Yii::$app->user->identity->name;
	    		$build['email'] = strlen($order->email) > 0 ? $order->email : Yii::$app->user->identity->email;
	    		$build['phone'] = strlen($order->phone) > 0 ? $order->phone : Yii::$app->user->identity->phone;
	    		$build['unp'] = $order->unp;
	    		$build['id'] = $order->id;
	    		
	    		$request['data'] = json_encode($build);
	    			
	    		$ch = curl_init("https://elksale.xyz/elksite.php");
		    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		    	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
		    	$body = curl_exec($ch);
		    	curl_close($ch);
			
	    		return $this->redirect(['/manage/orders']);
	    	}
    	}
    }
    public function actionCart_delete($id)
    {
    	$products = \Yii::$app->cart->getPositions();
    	foreach($products as $product)
    	{
    		if ($product->id == $id)
    		{
    			\Yii::$app->cart->remove($product);
    			break;
    		}
    	}
    	return $this->redirect(['cart']);
    }
    
    public function actionAdd_to_cart()
    {
    	//$cart = new ShoppingCart();
    	$posts = \Yii::$app->request->post();
    	
    	$model = Product::findOne($posts['id']);
    	if ($model) {
    		//$cart->put($model, $posts['amount']);
    		\Yii::$app->cart->put($model, $posts['amount']);
    		return $this->redirect(['index']);
    	}
    	throw new NotFoundHttpException();
    }
}
