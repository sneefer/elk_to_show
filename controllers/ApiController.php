<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Order;
use app\models\StoragesProducts;

class ApiController extends Controller
{
    public function actionIndex() {
        return '111';
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionOrder_delivered() {
        $req = json_decode(\Yii::$app->request->getRawBody());
        $order = Order::findOne(['id' => $req->order_id]);
        $orderContent = json_decode($order->products, true);

        foreach ($orderContent as $productId => $amountPrice) {
            list($amount, ) = $amountPrice;
            $stpr = StoragesProducts::findOne([
                'product_id' => $productId,
                'storage_id' => $order->storage_id,
            ]);
            if (!$stpr) {
                continue;
            }
            $stpr->count_real -= intval($amount);
            $stpr->save();
        }
    }
}
