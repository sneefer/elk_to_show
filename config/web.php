<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [ 
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Ivm5i9P5rFgAVk0DI13TQUQ_kswD3el6',
			'baseUrl' => ''
        ],
		'urlManager' => [
		  'showScriptName' => false,
		  'enablePrettyUrl' => true,
		  'rules' => [
			//'<controller>/<action>' => '<controller>/<action>'
			'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			'<controller:\w+>/<id:\d+>/<slug:\w+>' => '<controller>/view',
            '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/view',
            '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>/',
		  ]
		],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\BackendUser',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'basket' => [
			'class' => 'andreykluev\shopbasket\BasketComponent',
			'userClass' => 'common\models\BackendUser',
			'productClass' => 'common\models\Site',
			//'onLogin' => 'merge'
		],
		'cart' => [
			'class' => 'yz\shoppingcart\ShoppingCart',
			'cartId' => 'my_application_cart',
		]
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
			
    ],
    'params' => $params,
	'modules' => array(),
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    /*$config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];*/

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
