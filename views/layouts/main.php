<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use andreykluev\shopbasket\BasketAction;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<base href="<?=Url::base(true) ?>" />
    <?php $this->head() ?>
	<?php
		$this->registerCssFile('css/modules/datetimepicker/bootstrap-datetimepicker.min.css');
		//$this->registerCssFile('css/site.css');
	?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'ELK Shop',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Главная', 'url' => ['/']],
    		Yii::$app->user->isGuest ? 
    				( ['label' => 'Регистрация', 'url' => ['/site/register']] ) 
    				: (['label' => '', 'url' => ['/']]),
            //['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Авторизация', 'url' => ['/site/login']]
            ) : (
            	Yii::$app->user->identity->type == 2 ? 
				['label' => 'Заказы', 'url' => ['/manage/orders']]
            	: ['label' => 'Управление системой', 'url' => ['/manage/index']]
            ),
    		['label' => 'Товары', 'url' => ['/products/index'], 'visible' => !Yii::$app->user->isGuest],
    		['label' => 'Корзина ('.(\Yii::$app->cart->getCount() > 0 ? \Yii::$app->cart->getCount() : 'пусто').')', 'url' => ['/site/cart'], 'linkOptions' => ['id' => "cart_count"]],
			Yii::$app->user->isGuest == false ? (
				'<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Выйти (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            ) : ['label' => 'Товары', 'url' => '/products/index'],/*(
             
            )*/
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?php ?>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">Магазин ELK</p>
    </div>
</footer>

<?php $this->endBody() ?>
	<!-- JS: -->
	<?php $this->registerJsFile('js/modules/datetimepicker/bootstrap-datetimepicker.min.js'); ?>
	<?php $this->registerJsFile('js/modules/datetimepicker/locales/bootstrap-datetimepicker.ru.js'); ?>
	<?php $this->registerJsFile('js/modules/bootstrap_addons/bootstrap-confirmation.min.js'); ?>
	<?php $this->registerJsFile('js/common.js'); ?>
	<script type="text/javascript">
		$(document).ready(function(){
    		$('[data-toggle=confirmation]').confirmation({
  			  rootSelector: '[data-toggle=confirmation]',
  			  // other options
  			});
		});
	</script>
	<!-- end JS-->
</body>
</html>
<?php $this->endPage() ?>
