<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$products = \Yii::$app->cart->getPositions();

?>

<h1>Товары в корзине</h1>
<?php
						
	$form = ActiveForm::begin([
			'id' => 'cart-form',
			'options' => ['class' => 'form-horizontal'],
			'action' => 'site/cart_do'
	]) ?>
<table class="table table-stripped">
	<?php $html1 = "
		<tr>
			<th>Название</th>
			<th>Количество</th>
			<th>Стоимость</th>
			<th>Полная стоимость</th>
			<th>Удалить</th>
		</tr>
	";
	?>
	
	<?php
		$html2 = '';
		foreach($products as $product)
		{
			$html2 .= "<tr><td><a href='/products/view/" . $product->id . "'>" . $product->title . "</a>";
			$html2 .= "<td><input type='number' class='number cart_number' required='required' name=count[".$product->id."] value='".$product->quantity."' /></td>";
			$html2 .= "<td>".$product->price_sell."</td>";
			$html2 .= "<td>".($product->price_sell * $product->quantity)." BYN</td>";
			$html2 .= "<td><a class='btn btn-danger' href='/site/cart_delete/".$product->id."'>Удалить</td></tr>";
		}
		if (strlen($html2) > 0)
		{
			$html2 = $html1 . $html2;
		}
		else
		{
			$html2 = "козина пуста";
		}
		echo $html2;
	?>
</table>

<div class="row">
<?php 
	if (\Yii::$app->cart->getCount() > 0)
	{
		if (Yii::$app->user->isGuest)
		{
			echo "<b>Для того, чтобы оформить заказ Вам необходимо <a href='/site/register/'>зарегистрироваться</a></b>";
			/*
			
?>
			<div class="col-md-6">
			<?php
			
			$form = ActiveForm::begin([
					'id' => 'register-form',
					'options' => ['class' => 'form-horizontal'],
					'action' => 'site/doregister'
			]) ?>
			    <?= $form->field($model, 'username') ?>
			    <?= $form->field($model, 'password')->passwordInput() ?>
			    <?= $form->field($model, 'firstName') ?>
			    <?= $form->field($model, 'lastName') ?>
			    <?= $form->field($model, 'email') ?>
			
			    <?php 
			    	if ($errors != null)
			    	{
			    		$arr = [];
			    		foreach($errors as $key => $error)
			    		{
			    			$arr[] = $error[0];
			    		}
			    		echo implode(", ",$arr) . "<br /><br />";
			    	}
			    ?>
			    
			    <div class="form-group">
			        <div class="col-lg-offset-1 col-lg-11">
			            <?= Html::submitButton('Зарегистрироваться и оформить заказ', ['class' => 'btn btn-primary']) ?>
			        </div>
			    </div>
			<?php ActiveForm::end() ?>
			</div>
		<?php 
		*/}
		else
		{
			?>
				<div class="col-md-6">
				
				<?php
			    	if (count($stores) > 0)
			    	{
			    		echo "<div class='form-group'><b>Выберите склад:</b><br />";
			    	}
			    	
			    	$ind = 0;
					foreach($stores as $index=>$store)
					{
						$checked = $ind == 0 ? 'checked' : '';
						echo "<label><input type='radio' name='storesel' value='".$store->id."' ".$checked." /> ".$store->name."</label><br />";
						$ind++;
					}
					if (count($stores) > 0)
					{
						echo "</div>";
					}
			    ?>
			    <div class="form-group field-registerform-unp required">
					<label class="control-label" for="registerform-unp">УНП</label>
					<input type="text" id="registerform-unp" class="form-control" name="addfield[unp]" required="required" aria-required="true">
				
					<div class="help-block">Поле 'УНП' должно быть заполнено</div>
				</div>
				<div class="form-group field-registerform-unp required">
					<label class="control-label" for="registerform-unp">Телефон</label>
					<input type="text" id="registerform-unp" class="form-control" name="addfield[phone]" aria-required="false">
				
					<!--<div class="help-block">Поле 'Телефон' должно быть заполнено</div>-->
				</div>
				<div class="form-group field-registerform-unp required">
					<label class="control-label" for="registerform-unp">Email</label>
					<input type="text" id="registerform-unp" class="form-control" name="addfield[email]" aria-required="false">
				
					<!--<div class="help-block">Поле 'Email' должно быть заполнено</div>-->
				</div>
				<div class="form-group field-registerform-unp required">
					<label class="control-label" for="registerform-unp">ФИО</label>
					<input type="text" id="registerform-unp" class="form-control" name="addfield[uname]" aria-required="false">
				
					<!--<div class="help-block">Поле 'ФИО' должно быть заполнено</div>-->
				</div>
				<div class="form-group">
			        <div class="col-lg-offset-1 col-lg-11">
			            <?= Html::submitButton('Оформить заказ', ['class' => 'btn btn-primary']) ?>
			        </div>
			    </div>
			</div>
			<?php
		}
	}
?>
</div>

<?php ActiveForm::end() ?>
