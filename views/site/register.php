<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="col-md-6">
<?php

$form = ActiveForm::begin([
    'id' => 'register-form',
    'options' => ['class' => 'form-horizontal'],
	'action' => 'site/doregister'
]) ?>
	<?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'username') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'phone') ?>
    <?= $form->field($model, 'unp') ?>

    <?php 
    	if ($errors != null)
    	{
    		$arr = [];
    		foreach($errors as $key => $error)
    		{
    			$arr[] = $error[0];
    		}
    		echo implode(", ",$arr) . "<br /><br />";
    	}
    ?>
    
    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>
</div>