<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
?>

<div class="row">
	<!-- left sidebar -->
	<div class="col-md-3">
		<?php 
			//для обычных пользователей:
		echo Nav::widget([
				'options' => ['class' => 'navbar-nav navbar-left'],
				'items' => [
				['label' => 'Управление продуктами', 'url' => '/manage/index'],
				['label' => 'Управление заказами', 'url' => '/manage/manage_orders'],
				['label' => 'Управление складами', 'url' => '/manage/storages']
				]
			]);
		?>
	</div>
	
	<!-- content side -->
	<div class="col-md-9">
		<h1>Управление продуктами</h1>
		<a href="/manage/products_add/" class="btn btn-success">Добавить товар</a><br /><br />
		<table class="table table-striped table-products">
			<tr>
				<th>Фото</th>
				<th>Наименование</th>
				<th>Артикул</th>
				<th>Стоимость</th>
				<th>Количество</th>
				<th>Действия</th>
			</tr>
				<?php for($i=0; $i<count($products); $i++): ?>
				    <tr>
				    	<td>
				    		<img src="/img/<?=$products[$i]->photo ?>" class="product_photo" />
				    	</td>
				    	<td>
				    		<?=$products[$i]->title?>
				    	</td>
				    	<td>
				    		<?=$products[$i]->artid?>
				    	</td>
				    	<td>
				    		<?=$products[$i]->price_sell?>
				    	</td>
				    	<td>
				    		<?php 
				    			$summ_count = 0;
				    			foreach($products[$i]->stores as $store)
				    			{
				    				$summ_count += $store->count;
				    			}
				    			echo $summ_count;
				    		?>
				    		
				    	</td>
				    	<td>
				    		<!-- <a href="#" class="btn btn-default" data-title="Open Google?" data-toggle="confirmation">Удалить</a>
				    		-->
				    		<a class="btn btn-success" href="/manage/products_edit/<?=$products[$i]->id?>">Редактировать</a>
				    		<a class="btn btn-danger" data-toggle="confirmation" data-title="Удалить?"
   href="/manage/products_delete_do/<?=$products[$i]->id?>">Удалить</a>
				    	</td>
				    </tr>
				<?php endfor ?>
		</table>
	</div>
</div>