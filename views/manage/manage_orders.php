<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;

use app\models\Product;
use app\models\Storages;

?>

<!-- left sidebar -->
	<div class="col-md-3">
		<?php 
			//для обычных пользователей:
		echo Nav::widget([
				'options' => ['class' => 'navbar-nav navbar-left'],
				'items' => [
				['label' => 'Управление продуктами', 'url' => '/manage/index'],
				['label' => 'Управление заказами', 'url' => '/manage/manage_orders'],
				['label' => 'Управление складами', 'url' => '/manage/storages']
				]
			]);
		?>
	</div>
	
	<!-- content side -->
	<div class="col-md-9">
		<h1>Управление заказами:</h1>
		<table class="table table-stripped">
			<?php $html1 = "<tr>
				<th>Наименование</th>
				<th>Товары</th>
				<th>Дата заказа</th>
				<th>Склад</th>
				<th>Сумма заказа</th>
				<th>Действия</th>
			</tr>";
		
		
				$html2 = "";
				
				foreach($orders as $order)
				{
					$prod_tmp = json_decode($order->products,true,4);
					$prod_ids = array();
					foreach($prod_tmp as $id=>$prod)
					{
						$prod_ids[] = $id;
					}
					$products = Product::find(['id' => $prod_ids])->all();
					$products_html = "";
					$total_amount = 0;
					foreach($products as $product)
					{
						if (array_key_exists($product->id,$prod_tmp))
						{
							$current_summ = intval($prod_tmp[$product->id][0]) * $prod_tmp[$product->id][1];
							$total_amount += $current_summ;
							//$products_html .= "<a target='_blank' href='/products/view/" . $product->id . "'>".$product->title." (".$prod_tmp[$product->id][0]." шт. куплено<br /> за ".$prod_tmp[$product->id][1]." BYN/шт.)</a><br />";
							$products_html .= "<a target='_blank' href='/products/view/" . $product->id . "'>".$product->title."</a><br />";
						}
					}
					
					$store = Storages::findOne(['id' => $order->storage_id]);
					if ($store != null)
					{
						$store_name = $store->name;
					}
					else
					{
						$store_name = "";
					}
					
					$html2 .= "<tr>
							<td>Заказ №" . $order->id . "</td>" .
							"<td>" . $products_html . "</td>" .
							"<td>" . $order->created . "</td>" .
							"<td>" . $store_name . "</td>" .
							"<td>" . $order->summ . " BYN</td>" . 
							"<td>" . '<a href="/manage/manage_order_edit/'.$order->id.'" class="btn btn-success">Редактировать</a><a class="btn btn-danger" data-toggle="confirmation" data-title="Удалить?"
   href="/manage/manage_order_delete/'.$order->id . '">Удалить</a>' . "</td>" .
							"</tr>";
				}
				
				if (strlen($html2) > 0)
				{
					echo $html1.$html2;
				}
				else
				{
					echo "<tr><td colspan='5'>Пока заказов у Вас нет. <br /><a href='/products/index/'><b>Перейти в каталог</b></a></td></tr>";
				}
				
			?>
			
		</table>
	</div>