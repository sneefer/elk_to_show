<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
?>

<div class="row">
<!-- left sidebar -->
	<div class="col-md-3">
		<?php 
			//для обычных пользователей:
		echo Nav::widget([
				'options' => ['class' => 'navbar-nav navbar-left'],
				'items' => [
				['label' => 'Управление продуктами', 'url' => '/manage/index'],
				['label' => 'Управление заказами', 'url' => '/manage/manage_orders'],
				['label' => 'Управление складами', 'url' => '/manage/storages']
				]
			]);
		?>
	</div>

	<div class="col-md-9">
		<h1>Добавление склада</h1>
	<?php
	
	$form = ActiveForm::begin([
	    'id' => 'storage_add',
	    'options' => ['class' => 'form-horizontal'],
		'action' => 'manage/storages_add_do'
	]) ?>
		
		<?= $form->field($model, 'name') ?>
		<?= $form->field($model, 'address') ?>
		<?= $form->field($model, 'latitude') ?>
		<?= $form->field($model, 'longitude') ?>
	
		<div class="form-group">
			 <div class="col-lg-offset-1 col-lg-11">
			     <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
			 </div>
		</div>
		<?php ActiveForm::end() ?>
	</div>

</div>