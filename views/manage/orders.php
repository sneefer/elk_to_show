<?php

use app\models\Product;

?>

<h1>Мои заказы:</h1>

<table class="table table-stripped">
	<?php $html1 = "<tr>
		<th>Наименование</th>
		<th>Сумма заказа</th>
		<th>Дата заказа</th>
		<th>Дата обновления</th>
	</tr>";


		$html2 = "";
		$products_html = "";
		
		foreach($orders as $order)
		{
			$html2 .= "<tr>
					<td>Заказ №" . $order->id . "</td>" .
					"<td>" . $order->summ . "</td>" .
					"<td>" . $order->created . "</td>" .
					"<td>" . $order->updated . "</td>" .
					"</tr>";
		}
		
		if (strlen($html2) > 0)
		{
			echo $html1.$html2;
		}
		else
		{
			echo "<tr><td colspan='4'>Пока заказов у Вас нет. <br /><a href='/products/index/'><b>Перейти в каталог</b></a></td></tr>";
		}
		
	?>
	
</table>