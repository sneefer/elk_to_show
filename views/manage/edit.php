<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;

use unclead\multipleinput\MultipleInput;
?>

<!-- left sidebar -->
	<div class="col-md-3">
		<?php 
			//для обычных пользователей:
		echo Nav::widget([
				'options' => ['class' => 'navbar-nav navbar-left'],
				'items' => [
				['label' => 'Управление продуктами', 'url' => '/manage/index'],
				['label' => 'Управление заказами', 'url' => '/manage/manage_orders'],
				['label' => 'Управление складами', 'url' => '/manage/storages']
				]
			]);
		?>
	</div>
	
	<!-- content side -->
	<div class="col-md-9">
		<h1>Редактирование товара</h1>
		<?php

			$form = ActiveForm::begin([
			    'id' => 'add-new-product',
			    'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
				'action' => '/manage/products_edit_do/' . $model->id
			]) ?>
			    <?= $form->field($model, 'title') ?>
			    <?= $form->field($model, 'descr') ?>
			    <?= $form->field($model, 'artid') ?>
			    <?= $form->field($model, 'price_sell') ?>
			    <?= $form->field($model, 'price_buy') ?>
			    <?php
			    	if (count($stores) > 0)
			    	{
			    		echo "<div class='form-group'><b>Введите количесто товаров на складе:</b><br />";
			    		
			    	}
					
			    	foreach($stores_obj as $store)
			    	{
			    		//$store_prod = $store->getStoragesProducts();
			    		$elem = null;
			    		foreach($stpr as $one)
			    		{
			    			if ($one['product_id'] == $model->id && $one['storage_id'] == $store->id)
			    			{
			    				$elem = $one;
			    				break;
			    			}
			    		}
			    		if ($elem != NULL)
			    		{
			    			echo "<div class='col-md-12'><label class='control-label'>".$store->name."</label></div><div class='col-md-6'>Количество резерв<input type='number' required='required' class='form-control' name='stores[".$store->id."][count]' value='". (isset($elem['count']) ? $elem['count'] : 0) ."' /></div>".
			    																	"<div class='col-md-6'>Реальное кол-во: <input type='number' required='required' class='form-control' name='stores[".$store->id."][count_real]' value='". (isset($elem['count_real']) ? $elem['count_real'] : 0) ."' /></div>";
			    		}
			    	}
					if (count($stores) > 0)
					{
						echo "</div>";
					}
			    ?>
			    <?= $form->field($model, 'photo')->fileInput() ?>
			    <?php if (strlen($model->photo) > 0) { ?>
			    <img style="width: 90px;" src="<?php echo '/img/' . $model->photo;?>" /><br /><br />
				<?php } ?>
			    <?php 
			    	/*if ($errors != null)
			    	{
			    		$arr = [];
			    		foreach($errors as $key => $error)
			    		{
			    			$arr[] = $error[0];
			    		}
			    		echo implode(", ",$arr) . "<br /><br />";
			    	}*/
			    ?>
			    
			    <div class="form-group">
			        <div class="col-lg-offset-1 col-lg-11">
			            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
			            <?= Html::button('Отмена',['class' => 'btn', 'onclick' => "window.location.href= '/manage/index';"])?>
			        </div>
			    </div>
		<?php ActiveForm::end() ?>
	</div>
