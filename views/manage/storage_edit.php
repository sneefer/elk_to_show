<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;

use unclead\multipleinput\MultipleInput;
use app\models\Product;
use app\models\BackendUser;
?>

<!-- left sidebar -->
	<div class="col-md-3">
		<?php 
			//для обычных пользователей:
		echo Nav::widget([
				'options' => ['class' => 'navbar-nav navbar-left'],
				'items' => [
				['label' => 'Управление продуктами', 'url' => '/manage/index'],
				['label' => 'Управление заказами', 'url' => '/manage/manage_orders'],
				['label' => 'Управление складами', 'url' => '/manage/storages']
				]
			]);
		?>
	</div>
	
	<!-- content side -->
	<div class="col-md-9">


		<h1>Редактирование товаров склада #<?=$model->id?></h1>
		<?php

			$form = ActiveForm::begin([
			    'id' => 'add-new-product',
			    'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
				'action' => '/manage/storage_edit_do/' . $model->id
			]) ?>
				<div class="form-group">
					
					<label>Название склада:</label>
					<span><?=$model->name?></span><br />
					<label>Адрес:</label>
					<span><?=$model->address?></span><br />
					
				
				<label>Товары:</label>
					<table class="table table-striped">
						<tr>
							<th>
								Название
							</th>
							<th>
								Количество | Сумма
							</th>
						</tr>
				<?php 
					/*$prod_ids = array();
					$prods = json_decode($model->products,true);
					
					foreach($prods as $id=>$count)
					{
						$prod_ids[] = $id;
					}
					$products = Product::findAll(['id' => $prod_ids]);*/
					
					foreach($products as $product)
					{
						if (!empty($stprs[$product->id]))
						{
							echo "<tr>" . 
								"<td>" . $product->title . " (Артикул: " . $product->artid . ")" . "</td>" . 
								"<td><input style='width: 50px;' type='number' name='prods[".$product->id."]' value='" . $stprs[$product->id]->count . "' /> | <input type='text' name='prods_summ[".$product->id."]' value='" . $product->price_sell . "' /> BYN</td>";
							echo "</tr>";
						}
					}
				?>
					</table>
				
				</div>
			    
					    <div class="form-group">
			        <div class="col-lg-offset-1 col-lg-11">
			            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
			            <?= Html::button('Отмена',['class' => 'btn', 'onclick' => "window.location.href= '/manage/manage_orders';"])?>
			        </div>
			    </div>
		<?php ActiveForm::end() ?>
	</div>
	