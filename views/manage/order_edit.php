<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;

use unclead\multipleinput\MultipleInput;
use app\models\Product;
use app\models\BackendUser;
?>

<!-- left sidebar -->
	<div class="col-md-3">
		<?php 
			//для обычных пользователей:
		echo Nav::widget([
				'options' => ['class' => 'navbar-nav navbar-left'],
				'items' => [
				['label' => 'Управление продуктами', 'url' => '/manage/index'],
				['label' => 'Управление заказами', 'url' => '/manage/manage_orders'],
				['label' => 'Управление складами', 'url' => '/manage/storages']
				]
			]);
		?>
	</div>
	
	<!-- content side -->
	<div class="col-md-9">
		<h1>Редактирование заказа #<?=$model->id?></h1>
		<?php

			$form = ActiveForm::begin([
			    'id' => 'add-new-product',
			    'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
				'action' => '/manage/manage_order_edit_do/' . $model->id
			]) ?>
				<div class="form-group">
					<label>Наименование:</label>
					<span>Заказ №<?=$model->id?></span><br />
					<label>Дата заказа:</label>
					<span><?=$model->created?></span><br />
					<label>От пользователя:</label>
					<?php 
						echo $model->uname . " (" . $model->email . "), тел.: " . $model->phone . ", УНП: ".$model->unp;
					?><br />
				
				<label>Товары:</label>
					<table class="table table-striped" id="table_products">
						<tr>
							<th>
								Название
							</th>
							<th>
								Количество X Сумма
							</th>
							<th>
								Удалить
							</th>
						</tr>
				<?php 
					$prod_ids = array();
					$prods = json_decode($model->products,true);
					
					foreach($prods as $id=>$count)
					{
						$prod_ids[] = $id;
					}
					$products = Product::findAll(['id' => $prod_ids]);
					
					foreach($products as $product)
					{
						echo "<tr>" . 
							"<td>" . $product->title . " (Артикул: " . $product->artid . ")" . "</td>" . 
							"<td><input style='width: 50px;' type='number' name='prods[".$product->id."]' value='" . $prods[$product->id][0] . "' /> X <input type='text' name='prods_summ[".$product->id."]' value='" . $prods[$product->id][1] . "' /></td>".
							"<td><a href='#' class='btn btn-danger' onclick='$(this).parent().parent().remove(); return false;'>Удалить</a></td>";
						echo "</tr>";
					}
				?>
					</table>
					<a href="#" class="btn btn-info" data-toggle="modal" data-target="#myModal">Добавить в заказ ...</a>
				</div>
			    
					    <div class="form-group">
			        <div class="col-lg-offset-1 col-lg-11">
			            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
			            <?= Html::button('Отмена',['class' => 'btn', 'onclick' => "window.location.href= '/manage/manage_orders';"])?>
			        </div>
			    </div>
		<?php ActiveForm::end() ?>
	</div>

<script id="product_html" type="x-tmpl-mustache">
	<tr>
		<td>{#name#}</td>
		<td>
			<input style="width: 50px;" type="number" name="new_prods[{#id#}]" value="1"> X <input type="text" name="new_prods_summ[{#id#}]" value="{#price_sell#}">
		</td>
		<td>
			<a href="#" class="btn btn-danger" onclick="$(this).parent().parent().remove(); return false;">Удалить</a>
		</td>
	</tr>
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Выберите товар из списка</h5>
        
      </div>
      <div class="modal-body">
        	<table class="table table-striped">
        		<tr>
        			<th>Выбор</th>
        			<th>Картинка</th>
        			<th>Товар</th>
        		</tr>
        		<?php 
        			$products_arr = array();
        			$products = Product::find()->where(['not in', 'id', $prod_ids])->asArray()->all();
        			
        			foreach($products as $product)
        			{
        				$products_arr[$product['id']] = $product;
        				
        		?>
	        		<tr>
	        			<td><input type="checkbox" class="product_sel_chk" value="<?=$product['id']?>" /></td>
	        			<td><img src="/img/<?=$product['photo']?>" width="90px;" /></td>
	        			<td><?=$product['title']?></td>
	        		</tr>
	        	<?php 
        			} 
        		?>
        		
        	</table>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-primary" id="ready_btn" data-dismiss="modal">Выбрать</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
var data = <?php echo json_encode($products_arr);?>;

</script>