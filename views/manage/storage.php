<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
?>

<div class="row">
	<!-- left sidebar -->
	<div class="col-md-3">
		<?php 
			//для обычных пользователей:
		echo Nav::widget([
				'options' => ['class' => 'navbar-nav navbar-left'],
				'items' => [
				['label' => 'Управление продуктами', 'url' => '/manage/index'],
				['label' => 'Управление заказами', 'url' => '/manage/manage_orders'],
				['label' => 'Управление складами', 'url' => '/manage/storages']
				]
			]);
		?>
	</div>
	
	<!-- content side -->
	<div class="col-md-9">
		<h1>Управление складами</h1>
		<a href="/manage/storages_add/" class="btn btn-success">Добавить склад</a><br /><br />
		<table class="table table-striped table-products">
			<tr>
				<th>Название склада</th>
				<th>Адрес</th>
				<th>Широта</th>
				<th>Долгота</th>
				<th>Действия</th>
			</tr>
			<?php 
				foreach($storages as $one)
				{
					echo "<tr>";
						echo "<td><a href='/manage/storage_edit/".$one->id."'>".$one->name . "</a></td>" . "<td>" . $one->address . "</td>" . 
							 "<td>".$one->latitude . "</td>" . "<td>" . $one->longitude . "</td>";
			?>
					<td>
						<a class="btn btn-default" data-toggle="confirmation" data-title="Удалить?"
   href="/manage/storages_delete_do/<?=$one->id?>">Удалить</a>
					</td>
			<?php 
					echo "</tr>";
				}
			?>
		</table>
	</div>
</div>