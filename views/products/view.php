<h1><?=$product[0]->title?></h1>

<div class="row product_full">
    <div class="col-md-6"><img src="/img/<?=$product[0]->photo?>" />
        <div class="order">
            <form action="/ajax/add_to_cart/" method="post" class="prod_ajax">
            	<input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
           value="<?=Yii::$app->request->csrfToken?>" />
           		<input type="hidden" name="id" value="<?=$product[0]->id?>" />
                <div class="pull-left"><span class="red"><?=$product[0]->price_sell?> Р</span> x <input type="text" name="amount" value="1" /></div>
                <div class="pull-right"><input type="submit" value="Купить" /></div>
            </form>
        </div>
    </div>
    <div class="col-md-6"><br /><br /><div class="ltitle"><?=$product[0]->descr?></div>
    </div>
</div>