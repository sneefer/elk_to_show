<h1>Каталог товаров</h1>

<div class="row products">
<?php for($i=0; $i<count($products); $i++): ?>
    <div class="col-md-4">
        <a href="/products/view/<?=$products[$i]->id?>">
            <div class="product_small">
                <img src="/img/<?=$products[$i]->photo ?>" />
                <div class="product_title"><?=$products[$i]->title?></div> 
            </div>
        </a>
        <div class="order">
            <form action="/ajax/add_to_cart" method="post" class="prod_ajax">
			            	<input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
           value="<?=Yii::$app->request->csrfToken?>"/>
           					<input type="hidden" name="id" value="<?=$products[$i]->id?>" />
			                <div class="pull-left"><span class="red"><?=$products[$i]->price_sell?> Р</span> x <input type="text" name="amount" value="1" /></div>
			                <div class="pull-right"><input type="submit" value="Купить" /></div>
			</form>
        </div>
    </div>
<?php endfor ?>

</div>