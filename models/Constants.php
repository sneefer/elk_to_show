<?php
namespace app\models;

use Yii;
use yii\base\Model;
//Constants:

class Constants extends Model {
    
    //users types:

    public static $user_type_manager = 1;
    public static $user_type_simple = 2;
    public static $user_type_admin = 3;
    
    //order statuses:
    public static $order_status_new = 0;
    public static $order_status_accepted = 1;
    public static $order_status_ready = 2;
    
    
    public function rules()
    {
    	return [];
    }
}