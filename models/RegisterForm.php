<?php

namespace app\models;

use Yii;
use yii\base\Model;

class RegisterForm extends Model
{
	public $type;
	public $name;
	//public $lastName;
	public $username;
	public $password;
	public $email;
	public $phone;
	public $unp;
	
	public function rules()
	{
		return [
		[['username', 'password', 'unp', 'email', 'phone'], 'required', 'message' => "Поле '{attribute}' должно быть заполнено"],
		[['password'],'string', 'min' => 5, 'message' => 'Пароль должен содержать минимум {min} символов'],
		['email', 'email', 'message' => 'Email должен быть заполнен корректно'],
		[['email','name'],'default']
		];
	}
	
	public function attributeLabels()
	{
		return [
            'id' => 'ID',
            'type' => 'Тип',
            'created' => 'Дата создания',
            'name' => 'ФИО',
            //'lastName' => 'Фамилия',
            'username' => 'Логин пользователя',
            'password' => 'Пароль',
            'authKey' => 'Auth Key',
			'email' => 'Email',
			'phone' => 'Телефон',
			'unp' => 'УНП'
        ];
	}
	/*
	 передача и проверка полей для регистрации.
	 если ложь - вывод недостающего поля
	 истина - попытка регистрации
	 */
	public function register($fields)
	{
		//check fields:
		
	}
	
}