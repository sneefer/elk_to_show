<?php

namespace app\models;

use app\models\Constants;
use Yii;
use andreykluev\shopbasket\behaviors\BasketUserBehavior;

/**
 * This is the model class for table "backend_user".
 *
 */
class BackendUser extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
	
    public static function tableName()
    {
        return 'users';
    }

    public function behaviors()
	{
		return [
			//BasketUserBehavior::className(),
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['type','created','firstName', 'lastName', 'username', 'password', 'authKey','email'], 'required'],
        	[['type','unp'],'required'],
            [['name', 'username', 'password', 'authKey','email','phone'], 'string', 'max' => 100],
            [['username'], 'unique'],
            [['authKey'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'created' => 'Дата создания',
            'name' => 'ФИО',
            //'lastName' => 'Фамилия',
            'username' => 'Логин пользователя',
            'password' => 'Пароль',
            'authKey' => 'Auth Key',
			'email' => 'Email',
			'phone' => 'phone',
			'unp' => 'УНП'
        ];
    }
    
    public function getOrder()
    {
    	return $this->hasMany(Order::className(), ['customer_id' => 'id']);
    }

    public function haveAdminRights() {
		return ($this->type == Constants::$user_type_admin) || ($this->type == Constants::$user_type_manager);
	}

	public function haveUserOnlyRights() {
		return ($this->type == Constants::$user_type_simple);
	}



	public function getAuthKey()
	{
		return $this->authKey;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function validateAuthKey($authKey)
	{
		return $this->authKey === $authKey;
	}
	
	public static function findIdentity($id)
	{
		return self::findOne($id);
	}
	
	public static function findIdentityByAccessToken($token, $type = null)
	{
		//throw new \yii\base\NotSupportedException();
	}
	
	public static function findByUsername($username)
	{
		return self::findOne(['username' => $username]);
	}
	
	public function validatePassword($password)
	{
		return $this->password == md5($password."hU9sadm11");
	}
	
    public function onLogin()
    {

        return false;
    }
    
    public function hashPassword()
    {
    	$this->password = md5($this->password."hU9sadm11");
    }
}
