<?php

namespace app\models;

use Yii;

class StoragesProducts extends \yii\db\ActiveRecord
{
	/*public $product_id;
	public $storage_id;
	public $count;
	public $sku;*/
	
	public static function tableName()
	{
		return 'storages_products';
	}
	
	public function rules()
	{
		return [
			[['product_id','storage_id','count','count_real'],'required','message' => "Поле '{attribute}' должно быть заполнено"],
			[['sku'],'default']
			];
	}
	
	public function fields()
	{
		return [
		'storage', //will get getItemUnit method
		];
	}
	
	public function attributeLabels()
	{
		return [
			'id' => "ID",
			'product_id' => "Идентификатор продукта",
			'storage_id' => 'Идентификатор склада',
			'count' => 'Количество товаров в резерве',
			'count_real' => 'Реальное количество товаров',
			'sku' => 'Артикул товара'
		];
	}
	
	public function getProduct()
	{
		return $this->hasOne(Product::className(), ['product_id' => 'id']);
	}
	public function getProducts()
	{
		return $this->hasOne(Product::className(), ['id' => 'product_id']);
	}
	
	public function getStorage()
	{
		return $this->hasOne(Storages::className(), ['storage_id' => 'id']);
	}
}