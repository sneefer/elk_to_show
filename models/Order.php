<?php

namespace app\models;

use Yii;

class Order extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'orders';
	}
	
	public function rules()
	{
		return [
		[['status','products','userid','storage_id','products'],"required",'message' => "Поле '{attribute}' должно быть заполнено"],
		[['created','updated','name','summ','unp','phone','email','uname'],'default']
		];
	}
	
	public function attributeLabels()
	{
		return [
            'id' => 'ID',
            'created' => 'Дата создания',
            'updated' => 'Дата обновления',
            'name' => 'Название',
            'status' => 'Статус заказа',
            'storage_id' => 'Номер магазина',
            'products' => 'Список товаров с количеством',
            'userid' => 'Идентификатор пользователя',
            'summ' => 'Общая сумма заказа',
            'unp' => 'УНП',
            'phone' => 'Телефон',
            'email' => 'Email',
            'uname' => 'ФИО'
        ];
	}
	
	public function beforeDelete()
	{
		$prods = json_decode($this->products,true,4);
		foreach($prods as $id => $countAndPrice)
		{
			list($count, ) = $countAndPrice;
			if ($count > 0 && $id > 0)
			{
				$stpr = StoragesProducts::findOne(['product_id' => $id, 'storage_id' => $this->storage_id]);
				
				if ($stpr != null)
				{
					$stpr->count += intval($count);
					$stpr->save();
				}
			}
			
		}
		

		$request = $build =  array();
		$request['action'] = "delete_order";
		$request['order_id'] = $this->id;
		
		$ch = curl_init("https://elksale.xyz/elksite.php");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
		$body = curl_exec($ch);
		curl_close($ch);
		
		
		return true;
	}
}