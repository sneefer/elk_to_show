<?php

namespace app\models;

use Yii;

class Product extends \yii\db\ActiveRecord/* implements CartPositionInterface*/
{
	public $quantity = 0;
	public $storages;
	
	//use CartPositionTrait;
	public function getPrice()
	{
		return $this->price_sell;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function setQuantity($amount)
	{
		$this->quantity = $amount;
	}
	
	public function getQuantity()
	{
		return $this->quantity;
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    public function rules()
    {
        return [
            [['title','artid','price_buy','price_sell'],"required",'message' => "Поле '{attribute}' должно быть заполнено"],
            [['photo'], 'file'],
            [['created','updated','descr'],'default']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created' => 'Дата создания',
            'updated' => 'Дата обновления',
            'title' => 'Название товара',
            'descr' => 'Описание',
            'artid' => 'Артикул товара',
            'photo' => 'Фотография',
            'price_buy' => 'Стоимость закупки',
            'price_sell' => 'Стоимость продажи'
            /*'storeid' => 'Склад',
            'count' => 'Количество'*/
        ];
    }
    
    public function getStores()
    {
    	return $this->hasMany(StoragesProducts::className(), ['product_id' => 'id'])->andWhere("count != 0");
    }

    public function getProducts($limit = 0)
    {
        $limit = intval($limit);
        if ($limit > 0)
        {
            return $this->find()->limit($limit)->all();
        }
        else if ($limit == 0)
        {
            return $this->find()->all();
        }
        else
        {
            return false;
        }
    }
    
    public function getStorageProducts($store_id)
    {
    	if ($store_id > 0)
    	{
	    	$stores = StoragesProducts::find(['storage_id' => $store_id])
	    	->leftJoin("products","storages_products.product_id=products.id")
	    	->where(['!=', 'storages_products.count' , '0'])
	    	->andWhere(['=','storages_products.storage_id',$store_id])
	    	->all();
	    	//print_r($products);
	    	$data_products = array();
	    	foreach($stores as $store)
	    	{
	    		/*if ($store->count != 0)
	    		 {*/
	    		/*echo "STORE_ID:".$store->id."<br />";
	    		 echo "<pre>";
	    		print_r($store->products);
	    		echo "</pre>";*/
	    		//$data_products = $store->products;
	    		//}
	    		//print_r($store->products);
	    		$data_products[] = $store->products;
	    	}
	    	//print_r($stores);
	    	
	    	return $data_products;
    	}
    }

    public function getProduct($id)
    {
        if ($id > 0)
        {
            return $this->find()->where(['id' => $id])->all();
        }
        else
        {
            return null;
        }
    }
    
    public function beforeDelete()
    {
    	if (strlen($this->photo) > 0)
    	{
    		if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/img/".$this->photo))
    		{
    			unlink($_SERVER['DOCUMENT_ROOT'] . "/img/".$this->photo);
    		}
    	}
    	return true;
    }
    
}