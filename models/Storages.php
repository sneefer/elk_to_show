<?php

namespace app\models;

use Yii;
use app\models\Constants;

class Storages extends \yii\db\ActiveRecord
{
	/*public $name;
	public $address;
	public $latitude;
	public $longitude;
	
	public $storage_id;*/
	/**
	 * @inheritdoc
	 */

	public static function tableName()
	{
		return 'storages';
	}
	
	public function rules()
	{
		return [
			[['name'],'required','message' => "Поле '{attribute}' должно быть заполнено"],
			[['address','latitude','longitude'],'default']
		];
	}
	
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Название склада',
			'address' => 'Адрес склада',
			'latitude' => 'Широта',
			'longitude' => 'Долгота'
		];
	}
	
	public function getStoragesProducts()
	{
		return $this->hasOne(StoragesProducts::className(), ['storage_id' => 'id']);
		//return $this->hasOne(StoragesProducts::className(), ['storage_id' => 'id']);
	}
	
	public function beforeDelete()
	{
		StoragesProducts::updateAll(['count' => 0],'storage_id = '.$this->id);
		return true;
	}
}