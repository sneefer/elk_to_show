<?php

use yii\db\Migration;
use yii\db\Schema;

class m171006_080921_users extends Migration
{
    public function safeUp()
    {
		$this->createTable('users', [
			'id' => Schema::TYPE_PK,
			'type' => Schema::TYPE_SMALLINT,
			'created' => Schema::TYPE_DATETIME,
			'name' => Schema::TYPE_STRING,
			'username' => Schema::TYPE_STRING . " UNIQUE",
			'password' => Schema::TYPE_STRING,
			'authKey' => Schema::TYPE_STRING . " UNIQUE",
			'email' => Schema::TYPE_STRING . " UNIQUE",
			'phone' => Schema::TYPE_STRING,
			'unp' => Schema::TYPE_STRING . " UNIQUE"
		]);
    }

    public function safeDown()
    {
        $this->dropTable('users');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171006_080921_users cannot be reverted.\n";

        return false;
    }
    */
}
