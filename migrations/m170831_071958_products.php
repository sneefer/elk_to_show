<?php

use yii\db\Migration;
use yii\db\Schema;

class m170831_071958_products extends Migration
{
    public function safeUp()
    {
		$this->createTable('products', [
            'id' =>  Schema::TYPE_PK,
			'created' => Schema::TYPE_DATETIME,
			'updated' => Schema::TYPE_DATETIME,
            'title' => Schema::TYPE_STRING,
            'descr' => Schema::TYPE_STRING,
			'artid' => Schema::TYPE_STRING,
			'photo' => Schema::TYPE_TEXT,
			'price_buy' => Schema::TYPE_FLOAT,
			'price_sell' => Schema::TYPE_FLOAT,
			'storeid' => Schema::TYPE_INTEGER,
			'count' => Schema::TYPE_INTEGER . " DEFAULT 0"
        ]);
		
		$this->createTable('storages', [
			'id' =>  Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING,
			'address' => Schema::TYPE_TEXT,
			'latitude' => Schema::TYPE_STRING,
			'longitude' => Schema::TYPE_STRING
		]);
    }

    public function safeDown()
    {
        $this->dropTable('products');
        $this->dropTable('storages');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_071958_products cannot be reverted.\n";

        return false;
    }
    */
}
