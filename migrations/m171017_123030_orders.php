<?php

use yii\db\Migration;
use yii\db\Schema;

class m171017_123030_orders extends Migration
{
    public function safeUp()
    {
		$this->createTable('orders', [
			'id' =>  Schema::TYPE_PK,
			'created' => Schema::TYPE_DATETIME,
			'updated' => Schema::TYPE_DATETIME,
			'name' => Schema::TYPE_STRING,
			'status' => Schema::TYPE_SMALLINT,
			'storage_id' => Schema::TYPE_TEXT,
			'products' => Schema::TYPE_TEXT,
			'userid' => Schema::TYPE_INTEGER,
			'summ' => Schema::TYPE_FLOAT,
			'unp'  => Schema::TYPE_STRING,
			'phone'  => Schema::TYPE_STRING,
			'email'  => Schema::TYPE_STRING,
			'uname'  => Schema::TYPE_STRING
		]);
		
		$this->execute("ALTER TABLE orders AUTO_INCREMENT=12001;");
		
		$this->createTable('storages_products', [
			'id' => Schema::TYPE_PK,
			'sku' => Schema::TYPE_STRING,
			'product_id' => Schema::TYPE_INTEGER,
			'storage_id' => Schema::TYPE_INTEGER,
			'count'	 => Schema::TYPE_INTEGER,
			'count_real' => Schema::TYPE_INTEGER
		]);
    }

    public function safeDown()
    {
        $this->dropTable('orders');
        $this->dropTable('storages_products');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171017_123030_orders cannot be reverted.\n";

        return false;
    }
    */
}
